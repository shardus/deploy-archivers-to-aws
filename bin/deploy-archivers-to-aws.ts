#!/usr/bin/env node
import * as cdk from '@aws-cdk/core'
import { ArchiversStack } from '../lib/archivers-stack'
import { RegionSetupStack } from '../lib/region-setup-stack'

const app = new cdk.App()

// Define available regions
const regions = [
  'us-east-1', // 5016
  //'af-south-1', // 856 //we had problems with this region in recent testing
  'ap-east-1', // 856
  'eu-central-1', // 1112
  'eu-north-1', // 896
  'eu-south-1', // 856
  'eu-west-1', // 3000
  'eu-west-3', // 890
  'sa-east-1', // 3000
  'us-west-2', // 1112
]

// Get existingArchiverUrls and numArchivers
const existingArchiverUrls = process.env.existingArchiverUrls
if (!existingArchiverUrls) throw new Error('existingArchiverUrls required')

const numArchivers = Number(process.env.numArchivers)
if (!numArchivers) throw new Error('numArchivers required')

/**
 * Limit stacks to 10 nodes to stop resulting CloudFormation templates from
 * getting too big.
 */
const nodesPerStack = 10

// Divide numArchivers by nodesPerStack to get num of stacks needed.
const numStacks = Math.ceil(numArchivers / nodesPerStack)

// Get remainder for num nodes in last stack (if not perfectly divisible)
const nodesInLastStack = numArchivers % nodesPerStack

// Divide resulting stacks evenly amongst available regions.
const stacksPerRegion = Math.ceil(numStacks / regions.length)

// Get remainder for num stack in last region (if not perfectly divisible)
const stacksInLastRegion = numStacks % regions.length

// Create stacks
for (let r = 0; r < regions.length; r++) {
  const region = regions[r]

  // Setup region with vpc, 1 availability zone, and a security group
  const regionSetup = new RegionSetupStack(app, `RegionSetupStack-${region}`, {
    env: { region },
  })

  // In last region, use remainder values
  const isLast = r === regions.length - 1

  for (let i = 0; i < (isLast ? stacksInLastRegion : stacksPerRegion); i++) {
    new ArchiversStack(app, `ArchiversStack-${region}-${i}`, {
      env: { region },
      vpc: regionSetup.vpc,
      securityGroup: regionSetup.securityGroup,
      existingArchiverUrls,
      numNodes: isLast ? nodesInLastStack : nodesPerStack,
    })
  }
}

app.synth()
