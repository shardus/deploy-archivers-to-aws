# deploy-archivers-to-aws

## Usage

To start Archivers and have them join an existing network:
```
npm run deploy-archivers <EXISTING_ARCHIVER_URL> <NUM_ARCHIVERS_TO_DEPLOY>
```

To destroy all deployed Archivers:
```
npx cdk destroy
```

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
