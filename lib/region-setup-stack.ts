import * as cdk from '@aws-cdk/core'
import * as ec2 from '@aws-cdk/aws-ec2'

export interface RegionSetupStack extends cdk.Stack {
  vpc: ec2.Vpc
  securityGroup: ec2.SecurityGroup
}

export class RegionSetupStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create a vpc
    this.vpc = new ec2.Vpc(this, "VPC", {
      maxAzs: 1,
      subnetConfiguration: [
        {
          name: "Ingress",
          subnetType: ec2.SubnetType.PUBLIC
        }
      ]
    });

    // Create a security group
    this.securityGroup = new ec2.SecurityGroup(this, "SecruityGroup", {
      vpc: this.vpc
    });
    this.securityGroup.connections.allowFromAnyIpv4(ec2.Port.allTraffic());
  }
}