import * as ec2 from '@aws-cdk/aws-ec2'
import * as ecs from '@aws-cdk/aws-ecs'
import * as cdk from '@aws-cdk/core'
import { config } from 'dotenv'
import { resolve } from 'path'
import { DockerInstance } from './docker-instance-construct'
config({ path: resolve(__dirname, '../.secrets') })

interface ArchiversStackProps extends cdk.StackProps {
  numNodes: number
  existingArchiverUrls: string
  vpc?: ec2.Vpc
  securityGroup?: ec2.SecurityGroup
}
export class ArchiversStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props: ArchiversStackProps) {
    super(scope, id, props)

    /** Setup VPC and security group */

    const vpc = props.vpc
      ? props.vpc
      : new ec2.Vpc(this, 'VPC', {
          maxAzs: 1,
          subnetConfiguration: [
            {
              name: 'Ingress',
              subnetType: ec2.SubnetType.PUBLIC,
            },
          ],
        })

    const securityGroup = props.securityGroup
      ? props.securityGroup
      : new ec2.SecurityGroup(this, 'SecruityGroup', {
          vpc,
        })
    securityGroup.connections.allowFromAnyIpv4(ec2.Port.allTraffic())

    /** Create numNodes number of Archivers and pass them existingArchiverUrls */

    const environment: any = {}

    // Set env var to tell the Archivers their public IP addresses
    environment[this.node.tryGetContext('ArchiverIpAddrEnvVar')] = '`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`'

    // Set env var to tell the Archivers the urls of existing Archivers
    if (props.existingArchiverUrls && props.existingArchiverUrls != '') {
      environment['ARCHIVER_EXISTING'] = props.existingArchiverUrls
    }

    const archivers = []
    for (let i = 0; i < props.numNodes; i++) {
      archivers[i] = new DockerInstance(this, `Archiver${i}`, {
        imageUri: this.node.tryGetContext('ArchiverImageUri'),
        username: process.env.ArchiverRegistryUsername,
        token: process.env.ArchiverRegistryToken,
        environment,
        instanceType: new ec2.InstanceType('t3.large'),
        machineImage: ecs.EcsOptimizedImage.amazonLinux2(),
        vpc,
        instanceName: `Archiver${i}`,
        keyName: 'shardus-ent-cf',
        securityGroup,
        vpcSubnets: {
          subnetType: ec2.SubnetType.PUBLIC,
        },
      })
    }
    for (const archiver of archivers) console.log(archiver.instancePublicIp)
  }
}
