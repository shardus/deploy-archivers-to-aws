import { Construct } from '@aws-cdk/core'
import * as ec2 from '@aws-cdk/aws-ec2'
import { NoIamInstance } from './NoIamInstance'

interface DockerInstanceProps extends ec2.InstanceProps {
  imageUri: string
  username?: string
  token?: string
  environment?: { [name: string]: string }
}

export class DockerInstance extends NoIamInstance {
  constructor(scope: Construct, id: string, props: DockerInstanceProps) {
    super(scope, id, props)

    let env = ''
    if (props.environment) {
      env = Object.entries(props.environment)
        .map(([key, value]) => `--env ${key}=${value}`)
        .join(' ')
    }

    const userData = [`docker pull ${props.imageUri}`, `docker run -Pd --restart=always --network='host' ${env} ${props.imageUri}`]

    if (props.username && props.token) {
      userData.unshift(`for i in 1 2 3 4 5; do docker login ${props.imageUri.split('/')[0]} -u ${props.username} -p ${props.token} && break || sleep 10; done`)
    }

    this.addUserData(...userData)
  }
}
