import execa = require('execa')
const axios = require('axios')

async function sanityCheckExistingArchiverUrls(existingArchiverUrls: string) {
  let validArchivers = []
  let urls = existingArchiverUrls.split(",")
  for (let url of urls) {
    try {
      const res = await axios.get(`${url}/nodeinfo`)
      if (res.data.publicKey) {
        validArchivers.push({
          ip: res.data.ip,
          port: res.data.port,
          publicKey: res.data.publicKey
        })
      }

    } catch (e) {
      console.log('Error', e.message)
    }
  }
  return validArchivers
}

function sanityCheckNumArchivers(numArchivers: number) { 
  if (numArchivers > 0 && numArchivers < 100) return true
  else throw new Error("Invalid archiver number")
}

async function main() {
  // Get existingArchiverUrls and numArchivers
  const existingArchiverUrls = process.argv[2]
  const numArchivers = Number(process.argv[3])

  const ARCHIVER_EXISTING = await sanityCheckExistingArchiverUrls(existingArchiverUrls)
  if (ARCHIVER_EXISTING.length === 0) throw new Error('No active archiver found.')
  sanityCheckNumArchivers(numArchivers)

  // Set existingArchiverUrls and numArchivers as env vars and run `cdk deploy`
  execa.commandSync(`npx cdk deploy`, { stdio: [0, 1, 2], env: { existingArchiverUrls: JSON.stringify(ARCHIVER_EXISTING), numArchivers: numArchivers.toString() } })
}

main()
